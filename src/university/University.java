/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package university;

import campus.campus.ICampus;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import utilities.IObserver;
import utilities.ISubject;

/**
 *
 * @author Iustin
 */
public class University implements IUniversity, IObserver, ISubject {

    private final List<ICampus> campusList;
    private String name = "University UNKNOWN";

    private List<IObserver> observers;

    public University () {
        observers = new ArrayList<>();
        campusList = new ArrayList<>();

    }

    @Override
    public Boolean registerObserver (IObserver observer) {
        if ( observer == null ) {
            throw new NullPointerException("Cannot add a NULL IObserver object to"
                    + " a list of IObservers");
        }

        if ( observers.contains(observer) ) {
            return Boolean.FALSE;
        }

        return observers.add(observer);
    }

    @Override
    public Boolean removeObserver (IObserver observer) {
        if ( observer == null ) {
            throw new NullPointerException("Cannot remove a NULL IObserver "
                    + "object from a list of IObservers");
        }

        //if (observers.contains(observer))
        return observers.remove(observer);

        // return false;
    }

    @Override
    public void notifyObservers () {
        for ( int i = 0; i < observers.size(); i++ ) {
            observers.get(i).update(this);
        }

    }

    @Override
    public void update (ISubject iSubject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean addCampus (ICampus campus) {
        if ( campus == null ) {
            throw new NullPointerException("Cannot add a NULL ICampus object to"
                    + " a list of ICampus");
        }

        if ( campusList.contains(campus) ) {
            return false;
        }

        return this.campusList.add(campus);
    }

    @Override
    public boolean removeCampus (ICampus campus) {
        if ( campus == null ) {
            throw new NullPointerException("Cannot remove a NULL ICampus "
                    + "object from a list of ICampus");
        }

        if ( campusList.contains(campus) ) {
            return campusList.remove(campus);
        }

        return false;
    }

    @Override
    public boolean hasCampus (ICampus campus) {
        if ( null == campus ) {
            throw new NullPointerException();
        }
        return campusList.contains(campus);
    }

    @Override
    public Iterator getCampuses () {
        return this.campusList.iterator();
    }

    /**
     * @return the name
     */
    @Override
    public String getName () {
        return name;
    }

    /**
     * @param name the name to set
     */
    @Override
    public void setName (String name) {
        this.name = name;
        notifyObservers();
    }

    @Override
    public String toString () {
        return getName();
    }
}
