/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package university;

import campus.campus.ICampus;
import java.util.Iterator;

/**
 *
 * @author Iustin
 */
public interface IUniversity {
    
    public boolean addCampus(ICampus campus);
    public boolean removeCampus(ICampus campus);
    public boolean hasCampus(ICampus campus);
    
    public Iterator getCampuses();

    public void setName(String name);
    public String getName();
    


}
