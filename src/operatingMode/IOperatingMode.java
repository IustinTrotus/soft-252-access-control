/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operatingMode;

/**
 *
 * @author Iustin
 */
public interface IOperatingMode {    
    
    public OperatingMode getOperatingMode();
    public void setOperatingMode(OperatingMode operatingMode);
    public void setOperatingMode(OperatingMode operatingMode,boolean forceEmergencyMode);
    
}
