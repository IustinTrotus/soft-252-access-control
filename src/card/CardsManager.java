/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package card;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Iustin
 */
public class CardsManager {

    private List<ICard> cards;

    public CardsManager() {
        cards = new ArrayList<>();
    }

    public boolean addCard(ICard card) {
        if (null == card) {
            throw new NullPointerException("Cannot add a null ICard to the list of cards");
        }

        if (cards.contains(card)) {
            return false;
        }
       return cards.add(card);
    }

    public boolean deleteCard(ICard card) {
        if (null == card) {
            throw new NullPointerException("Cannot delete a null ICard from the list of cards");
        }
        if (cards.contains(card)) {
            return cards.remove(card);
        }
        return false;
    }

}
