/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package card;

/**
 *
 * @author Iustin
 */
public interface ICardsManager {
    public boolean addCard(ICard card);
    public boolean deleteCard(ICard card);
    
}
