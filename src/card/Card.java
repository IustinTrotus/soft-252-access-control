/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package card;

import user.IUser;

/**
 *
 * @author Iustin
 */
public class Card implements ICard {

    IUser owner;
    long cardNumber;

    private Card() {
        this.cardNumber = 0;
    }

    public Card(IUser myOwner) {
        this.cardNumber = 0;
        owner = myOwner;
    }

    @Override
    public IUser getOwner() {
        return owner;
    }

    @Override
    public void setNumber(long number) {
        if (number<0) {
            throw new IllegalArgumentException();
        }
        this.cardNumber=number;
    }

    @Override
    public long getNumber() {
        return this.cardNumber;
    }

}
