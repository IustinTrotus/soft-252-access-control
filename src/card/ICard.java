/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package card;

import user.IUser;

/**
 *
 * @author Iustin
 */
public interface ICard {
    
    IUser getOwner();
    
    public void setNumber(long number);
    public long getNumber();
}
