/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accessControlModel;

import java.util.ListIterator;
import university.IUniversity;

/**
 *
 * @author Iustin
 */
public interface IAccessControlModel {
    ListIterator getUniversities();
    
    boolean addUniversity (IUniversity university);
}
