/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package accessControlModel;

import campus.building.Building;
import campus.building.IBuilding;
import campus.campus.Campus;
import campus.campus.ICampus;
import campus.room.IRoom;
import campus.room.SecureRoom;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import university.IUniversity;
import university.University;

/**
 *
 * @author Iustin
 */
public class AccessControlModel implements IAccessControlModel {

    private List<IUniversity> universityList;

    public AccessControlModel () {
        this.universityList = new ArrayList<>();

        IUniversity uni = new University();
        uni.setName("Plymouth Uni");

        universityList.add(uni);

        uni = new University();
        uni.setName("City College");
        universityList.add(uni);

        for ( IUniversity university : universityList ) {
            for ( int i = 1; i < 5; i++ ) {
                ICampus campus = new Campus();
                campus.setName("Campus "+i);
                
                for ( int j = 1; j < 30; j++ ) {

                    IBuilding building = new Building();
                    building.setCode("b" + j);
                    building.setName("Building " + j);

                    for ( int k = 1; k < 65; k++ ) {
                        IRoom room = new SecureRoom();
                        room.setName(building.getCode() + " Room "+k);
                        building.addRoom(room);
                        
                        
                    }
                    campus.addBuilding(building);
                }

                university.addCampus(campus);

            }
        }
    }

    @Override
    public ListIterator getUniversities () {
        return this.universityList.listIterator();
    }

    @Override
    public boolean addUniversity (IUniversity university) {
        if ( university == null ) {
            throw new NullPointerException();
        }
        if ( universityList.contains(university) ) {
            return false;
        }
        return universityList.add(university);
    }

}
