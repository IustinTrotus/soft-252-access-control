/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fileHandler;

import campus.building.Building;
import campus.campus.Campus;
import campus.room.LectureHall;
import campus.room.ResearchLab;
import campus.room.Room;
import campus.room.SecureRoom;
import campus.room.StaffRoom;
import campus.room.StudentLab;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import university.University;
import operatingMode.ModeManager;

/**
 *
 * @author Iustin
 */
public class FileHandler {

    private String fileName = "Soft252.xml";
    
    private static FileHandler self = null;
    
    private FileHandler() {
    }
    
    public static FileHandler getInstance(){
    
        if (null==FileHandler.self) {
            FileHandler.self = new FileHandler();
        }
        return FileHandler.self;
    }
       
    private XStream prepareXStream(){
         XStream xstream = new XStream(new DomDriver());
          
         xstream.alias("Umiversity", University.class);
         xstream.alias("Campus", Campus.class);
         xstream.alias("Building", Building.class);
         
         xstream.alias("Room", Room.class);         
         xstream.alias("StudentLab", StudentLab.class);
         xstream.alias("ResearchLab", ResearchLab.class);
         xstream.alias("SecureRoom", SecureRoom.class);
         xstream.alias("StaffRoom", StaffRoom.class);
         xstream.alias("LectureHall", LectureHall.class);
         
         
         xstream.useAttributeFor(Campus.class, "name");
         
         xstream.useAttributeFor(Building.class, "name");
         xstream.useAttributeFor(Building.class, "code");
         
         xstream.useAttributeFor(Room.class, "name");
         xstream.useAttributeFor(Room.class, "roomType");
         
         xstream.useAttributeFor(ModeManager.class, "operatingMode");
         
        xstream.addImplicitCollection(University.class, "campusList");
        /**
        xstream.addImplicitCollection(Campus.class, "buildingsList");
        xstream.addImplicitCollection(Building.class, "rooms");
                
        **/
        
         return xstream;
         }
    
    
    public void serialiseObject( Object object,String filename){
        XStream xstream = prepareXStream();
        
        //Write to a file in the file system
        try {
            FileOutputStream fs = new FileOutputStream(fileName);
            xstream.toXML(object, fs);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        
    }
    
    
    public University deserialiseObject(String filename){
    
        XStream xstream = prepareXStream();
       
        University object =  new University();

        try {
            FileInputStream fis = new FileInputStream(fileName);
            xstream.fromXML(fis, object);

        } catch (FileNotFoundException ex) {
        }
        
    return object;
    }
    
    private boolean fileExists(String fileName){
    FileHandler file = new FileHandler();
    
    return file.fileExists(fileName);
    }


     
}
