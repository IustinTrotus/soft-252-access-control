/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logger;

import java.time.LocalDateTime;

/**
 *
 * @author Iustin
 */
public class Log {
    private LogType logType;
    private String logContent;
    private LocalDateTime dateTime;
    
    public Log(LogType logType, String logContent) {
        dateTime = LocalDateTime.now();
        setLogType(logType);
        setLogContent(logContent);
    }

    /**
     * @return the logType
     */
    public LogType getLogType() {
        return logType;
    }

    /**
     * @param logType the logType to set
     */
    public void setLogType(LogType logType) {
        this.logType = logType;
    }

    /**
     * @return the logContent
     */
    public String getLogContent() {
        return logContent;
    }

    /**
     * @param logContent the logContent to set
     */
    public void setLogContent(String logContent) {
        this.logContent = logContent;
    }

    /**
     * @return the dateTime
     */
    public LocalDateTime getDateTime() {
        return dateTime;
    }

    /**
     * @param dateTime the dateTime to set
     */
    private void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
    
}
