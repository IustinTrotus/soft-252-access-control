/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logger;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Iustin
 */
public class LogsManager {

    private List<Log> logs;
    private static LogsManager self = null;

    private LogsManager() {
        this.logs = new ArrayList<>();
    }

    public static LogsManager getInstance() {

        if (null == LogsManager.self) {
            LogsManager.self = new LogsManager();
        }

        return LogsManager.self;
    }

    public ListIterator getLogs() {
        return this.logs.listIterator();
    }

    private void addLog(Log log) {
        this.logs.add(log);
    }

    public void createLog_OperatingMode(String type, String name, String operatingMode) {

        String logContent = logContentConstructor_OperatingMode(type, name, operatingMode);
        Log log = new Log(LogType.OperatingModeChanged, logContent);
        addLog(log);
    }

    public void createLog_Access(String roomName, String operatingMode,
            String userName, Long cardNumber, boolean accessGranted) {

        String logContent = logContentConstructor_EntryLog(roomName, operatingMode, userName, cardNumber, accessGranted);

        Log log = new Log(LogType.OperatingModeChanged, logContent);
        addLog(log);
    }

    private String logContentConstructor_EntryLog(String roomName,
            String operatingMode, String userName, Long cardNumber, boolean accessGranted) {

        String content = roomName + " " + operatingMode + " " + userName + " " + cardNumber + " " + accessGranted;
        return content;
    }

    private String logContentConstructor_OperatingMode(String type, String name, String operatingMode) {
        String typeFormated = type.substring(0, 1).toUpperCase() + type.substring(1);

        String content = typeFormated + " " + name + " entered " + operatingMode + " mode";
        return content;
    }
}
