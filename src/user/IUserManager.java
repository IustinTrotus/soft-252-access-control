/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user;

/**
 *
 * @author Iustin
 */
public interface IUserManager {
    public boolean addUser(IUser user);
    public boolean removeUser(IUser user);
}
