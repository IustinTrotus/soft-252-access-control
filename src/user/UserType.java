/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user;

/**
 *
 * @author Iustin
 */
public enum UserType {
    EmergencyResponder,
    Security,
    Manager,
    Student,
    Staff,
    ContractCleaner,
    Visitor
}
