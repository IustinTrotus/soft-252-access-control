/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Iustin
 */

public class UsersManager implements IUserManager{
    private List<IUser> usersList;

    public UsersManager() {
        
        usersList = new ArrayList<>();
    }
    
    @Override
    public boolean addUser(IUser user){
        if (null==user) {
            throw new NullPointerException("cannot add a null IUser to the list of users");
        }
        if (usersList.contains(user)) {
            return false;
        }
        return usersList.add(user);
    }
    
    @Override
    public boolean removeUser(IUser user){
     if (null==user) {
            throw new NullPointerException("cannot remove a null IUser from the list of users");
        }
        return usersList.remove(user);
    }
    
    
}
