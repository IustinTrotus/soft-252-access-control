/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user;

import java.util.Iterator;
import role.Role;

/**
 *
 * @author Iustin
 */
public interface IUser {
    
   String getName();
   void setName(String userNAme);
   
   
    public Iterator getRoles();
    public boolean addRole(Role role);
    
}
