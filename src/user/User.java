package user;

import card.Card;
import card.ICard;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import role.Role;

/**
 *
 * @author Iustin
 */
public abstract class User implements IUser {

    private String userName;
    private List<Role> roles;
    private ICard card;

    private User() {
    }

    public User(List<Role> userRoles) {
        this.roles = new ArrayList<>();

        userRoles.stream().forEach((userRole) -> {
            this.roles.add(userRole);
        });
        
        card=new Card(this);
    }

    /**
     * @return the userName
     */
    @Override
    public String getName() {
        return userName;
    }

    /**
     * @param userNAme the userName to set
     */
    @Override
    public void setName(String userNAme) {
        this.userName = userNAme;
    }

    @Override
    public Iterator getRoles() {
        return (roles.iterator());
    }

    @Override
    public boolean addRole(Role role) {
        if (role == null) {
            throw new NullPointerException();
        }
        if (this.roles.contains(role)) {
            return false;
        }
        return this.roles.add(role);
    }

}
