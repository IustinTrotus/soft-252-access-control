/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package campus.campus;

import campus.building.Building;
import campus.building.IBuilding;
import card.CardsManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import logger.LogsManager;
import operatingMode.IOperatingMode;
import operatingMode.ModeManager;
import operatingMode.OperatingMode;
import role.RolesManager;
import user.UsersManager;
import utilities.IObserver;
import utilities.ISubject;

/**
 *
 * @author Iustin
 */
public class Campus implements ICampus, IOperatingMode, ISubject, IObserver {

    private List<IBuilding> buildingsList;
    private String name;
    private ModeManager modeManager;
    private List<IObserver> observers;

    private final RolesManager rolesManager;
    private LogsManager logsManager;

    private final CardsManager cardsManager;
    private final UsersManager usersManager;

    public Campus () {
        logsManager = LogsManager.getInstance();

        buildingsList = new ArrayList<>();
        modeManager = new ModeManager();
        observers = new ArrayList<>();

        cardsManager = new CardsManager();
        rolesManager = new RolesManager();
        usersManager = new UsersManager();
    }

    @Override
    public void setName (String name) {
        if ( name == null ) {
            throw new NullPointerException();
        }

        if ( name.isEmpty() ) {
            throw new IllegalArgumentException();
        }

        this.name = name.toUpperCase();
        notifyObservers();
    }

    @Override
    public String getName () {
        return this.name;
    }

    @Override
    public boolean addBuilding (IBuilding building) {
        if ( building == null ) {
            throw new NullPointerException("Cannot add a NULL Building object to"
                    + " a list of IBuilding");
        }

        if ( buildingsList.contains(building) ) {
            return false;
        }
        
        registerObserver((IObserver)building);
        return buildingsList.add(building);
    }

    @Override
    public boolean removeBulding (Building building) {
        if ( building == null ) {
            throw new NullPointerException("Cannot remove a NULL Building "
                    + "object from a list of IBuildings");
        }

        if ( buildingsList.contains(building) ) {
            return buildingsList.remove(building);
        }

        return true;
    }

    /**
     *
     * @return
     */
    @Override
    public Iterator getBuildingsList () {
        return this.buildingsList.iterator();
    }

    @Override
    public OperatingMode getOperatingMode () {
        return modeManager.getOperatingMode();
    }

    @Override
    public void setOperatingMode (OperatingMode operatingMode) {
        modeManager.setOperatingMode(operatingMode);
        notifyObservers();
    }

    @Override
    public void setOperatingMode (OperatingMode operatingMode, boolean forceEmergencyMode) {
        modeManager.setOperatingMode(operatingMode, forceEmergencyMode);
    }

    //########### IObserver start
    @Override
    public void update (ISubject iSubject) {

        IOperatingMode iSubjecOpM = ( IOperatingMode ) iSubject;
        OperatingMode subjectOpM = iSubjecOpM.getOperatingMode();

        this.setOperatingMode(subjectOpM);

    }

    //########### ISubject start
    @Override
    public Boolean registerObserver (IObserver observer) {
        if ( observer == null ) {
            throw new NullPointerException("Cannot add a NULL IObserver object to"
                    + " a list of IObservers");
        }

        if ( observers.contains(observer) ) {
            return Boolean.FALSE;
        }

        return observers.add(observer);
    }

    @Override
    public Boolean removeObserver (IObserver observer) {
        if ( observer == null ) {
            throw new NullPointerException("Cannot remove a NULL IObserver "
                    + "object from a list of IObservers");
        }

        //if (observers.contains(observer))
        return observers.remove(observer);

        // return false;
    }

    @Override
    public void notifyObservers () {
        for ( int i = 0; i < observers.size(); i++ ) {
            observers.get(i).update(this);
        }

    }

    @Override
    public String toString () {
        return getName();
    }

}
