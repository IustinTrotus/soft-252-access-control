/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package campus.room;

import java.util.List;
import role.Role;
import user.UserType;

/**
 *
 * @author Iustin
 */
public class SecureRoom extends Room{
    public SecureRoom(  ) {
        super(RoomType.SECURE_ROOM );
    }
    

    @Override
    protected boolean canAccess(List<Role> roles) {
        
          return roles.stream().anyMatch((role) 
                -> (role.getUserType()==UserType.Manager
                        || 
                        role.getUserType()==UserType.Security));
    }


}
