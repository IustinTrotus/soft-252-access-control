/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package campus.room;

/**
 *
 * @author Iustin
 */
public enum RoomType {
    LECTURE_HALL,
    STUDENT_LAB,
    RESEARCH_LAB,
    STAFF_ROOM,
    SECURE_ROOM;
}
