/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package campus.room;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import logger.LogsManager;
import operatingMode.IOperatingMode;
import operatingMode.ModeManager;
import operatingMode.OperatingMode;
import role.Role;
import user.IUser;
import user.UserType;
import utilities.IObserver;
import utilities.ISubject;

/**
 *
 * @author Iustin
 */
public abstract class Room implements IRoom, IOperatingMode, IObserver, ISubject {

    private RoomType roomType;
    private IOperatingMode modeManager;
    private String name;
    private List<IObserver> observers;
    private LogsManager logsManager;

    private Room () {
    }

    public Room (RoomType roomType) {
        this.roomType = roomType;
        logsManager = LogsManager.getInstance();

        modeManager = new ModeManager();
        this.name = "Unknown";
        observers = new ArrayList<>();
    }

    public boolean requestAccess (IUser iUser) {

        List<Role> roles = new ArrayList<>();

        for ( Iterator<Role> iterator = iUser.getRoles(); iterator.hasNext(); ) {
            Role role = iterator.next();
            roles.add(role);
        }

        if ( getOperatingMode() == OperatingMode.Emergency ) {
            return canAccessEmergency(roles);
        }
        else {
            //TODO: emergency responder might be able to enter
            return canAccess(roles);
        }
    }

    protected boolean canAccess (List<Role> roles) {

        return roles.stream().filter((role) -> (role.getUserType() != UserType.EmergencyResponder)).anyMatch((role) -> (role.isInTimeSlot()));
    }

    protected boolean canAccessEmergency (List<Role> roles) {

        return roles.stream().anyMatch((role)
                -> (role.getUserType() == UserType.EmergencyResponder
                || role.getUserType() == UserType.Security));
    }

    @Override
    public OperatingMode getOperatingMode () {
        return this.modeManager.getOperatingMode();
    }

    @Override
    public void setOperatingMode (OperatingMode operatingMode) {
        this.modeManager.setOperatingMode(operatingMode);
        notifyObservers();
    }

    @Override
    public void setOperatingMode (OperatingMode operatingMode, boolean forceEmergencyMode) {
        modeManager.setOperatingMode(operatingMode, forceEmergencyMode);
        notifyObservers();
    }

    @Override
    public void update (ISubject iSubject) {
        IOperatingMode subject = ( IOperatingMode ) iSubject;
        OperatingMode operatingMode = subject.getOperatingMode();
 
            this.setOperatingMode(operatingMode); 
    }

    @Override
    public void setName (String name) {
        if ( null == name ) {
            throw new NullPointerException();
        }

        if ( name.isEmpty() ) {
            throw new IllegalArgumentException();
        }
        this.name = name;

    }

    @Override
    public String getName () {
        return this.name;
    }

    @Override
    public Boolean registerObserver (IObserver observer) {
        if ( observer == null ) {
            throw new NullPointerException("Cannot add a NULL IObserver object to"
                    + " a list of IObservers");
        }

        if ( observers.contains(observer) ) {
            return false;
        }

        return observers.add(observer);

    }

    @Override
    public Boolean removeObserver (IObserver observer) {
        if ( observer == null ) {
            throw new NullPointerException("Cannot remove a NULL IObserver "
                    + "object from a list of IObservers");
        }

        return observers.remove(observer);

    }

    @Override
    public void notifyObservers () {
        for ( int i = 0; i < observers.size(); i++ ) {
            observers.get(i).update(this);
        }
    }

    @Override
    public String toString () {
        return getName();
    }
}
