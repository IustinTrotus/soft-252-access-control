/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package campus.building;

import campus.room.IRoom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import logger.LogsManager;
import operatingMode.IOperatingMode;
import operatingMode.ModeManager;
import operatingMode.OperatingMode;
import utilities.IObserver;
import utilities.ISubject;

/**
 * @author Iustin
 */
public class Building implements IBuilding, IOperatingMode, IObserver, ISubject {

    private String name, code;
    private List<IRoom> rooms;
    private List<IObserver> observers;
    private ModeManager modeManager;
    private int roomsInEmergency = 0;
    private LogsManager logsManager;

    public Building () {
        logsManager = LogsManager.getInstance();

        rooms = new ArrayList<>();
        observers = new ArrayList<>();
        modeManager = new ModeManager();
    }

    @Override
    public OperatingMode getOperatingMode () {
        return modeManager.getOperatingMode();
    }

    @Override
    public void setOperatingMode (OperatingMode operatingMode) {
        modeManager.setOperatingMode(operatingMode);
        notifyObservers();
    }

    @Override
    public void setOperatingMode (OperatingMode operatingMode, boolean forceEmergencyMode) {
        modeManager.setOperatingMode(operatingMode, forceEmergencyMode);
        notifyObservers();
    }

    @Override
    public void update (ISubject iSubject) {
        IOperatingMode subject = ( IOperatingMode ) iSubject;
        OperatingMode operatingMode = subject.getOperatingMode();

             this.setOperatingMode(operatingMode);
        }

    @Override
    public Boolean registerObserver (IObserver observer) {
        if ( observer == null ) {
            throw new NullPointerException("Cannot add a NULL IObserver object to"
                    + " a list of IObservers");
        }

        if ( observers.contains(observer) ) {
            return false;
        }

        return observers.add(observer);
    }

    @Override
    public Boolean removeObserver (IObserver observer) {
        if ( observer == null ) {
            throw new NullPointerException("Cannot remove a NULL IObserver "
                    + "object from a list of IObservers");
        }

        //if (observers.contains(observer))
        return observers.remove(observer);

        // return false;
    }

    @Override
    public void notifyObservers () {
        for ( int i = 0; i < observers.size(); i++ ) {
            observers.get(i).update(this);
        }

    }

    /**
     * @return the rooms
     */
    @Override
    public Iterator getAllRooms () {
        return rooms.iterator();
    }

    @Override
    public boolean addRoom (IRoom room) {
        if ( room == null ) {
            throw new NullPointerException("Cannot add a NULL Room object to"
                    + " a list of IRoom");
        }

        if ( rooms.contains(room) ) {
            return false;
        }
        this.registerObserver((IObserver)room);
        return rooms.add(room);
    }

    @Override
    public boolean removeRoom (IRoom room) {
        if ( room == null ) {
            throw new NullPointerException("Cannot remove a NULL Room "
                    + "object from a list of IRooms");
        }

        if ( rooms.contains(room) ) {
            return rooms.remove(room);
        }
        removeObserver((IObserver)room);
        return true;
    }

    @Override
    public void setName (String name) {
        if ( name == null ) {
            throw new NullPointerException();
        }

        if ( name.isEmpty() ) {
            throw new IllegalArgumentException();
        }

        this.name = name.toUpperCase();
    }

    @Override
    public String getName () {
        return this.name;
    }

    @Override
    public void setCode (String code) {
        if ( code == null ) {
            throw new NullPointerException();
        }

        if ( code.isEmpty() ) {
            throw new IllegalArgumentException();
        }

        this.code = code.toUpperCase();
    }

    @Override
    public String getCode () {
        return this.code;
    }

    /**
     * @return the roomsInEmergency
     */
    public int getRoomsInEmergency () {
        return roomsInEmergency;
    }

    @Override
    public String toString () {
        return getName();
    }
}
