/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package campus.building;

import campus.room.IRoom;
import java.util.Iterator;

/**
 *
 * @author Iustin
 */
public interface IBuilding {
    boolean addRoom(IRoom room);
    boolean removeRoom(IRoom room);
    
    Iterator getAllRooms();
    
    void setName(String name);
    String getName();
    
    void setCode(String name);
    String getCode();
            
}
