/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 * An interface to make a class a "subject which can be observed"
 */
public interface ISubject {
    
    Boolean registerObserver(IObserver observer);
    Boolean removeObserver(IObserver observer);
    void notifyObservers();
    
    @Override
    public String toString ();
    
}
