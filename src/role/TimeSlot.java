
package role;

import java.time.Duration;
import java.time.LocalTime;


/**
 *
 * @author Iustin
 */
public class TimeSlot {
    private LocalTime startTime ;
    private Duration duration;

    private TimeSlot() {
    } 

    public TimeSlot(LocalTime startTime, Duration duration) {
        this.startTime = startTime;
        this.duration = duration;
    }
    

 public LocalTime getEndTime(){
    return this.getStartTime().plusSeconds(getDuration().getSeconds());
    }

    /**
     * @return the startTime
     */
    public LocalTime getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the duration
     */
    public Duration getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(Duration duration) {
        this.duration = duration;
    }
}
