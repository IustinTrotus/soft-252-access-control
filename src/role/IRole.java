/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package role;

import user.UserType;

/**
 *
 * @author Student_SA
 */
interface IRole {
    
    public UserType getUserType();
    public void setUserType(UserType userType);
    
    public TimeSlot getTimeSlot();
    public void setTimeSlot(TimeSlot timeSlot);
    
    public boolean isInTimeSlot();
    
    
    
}
