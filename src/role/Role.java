/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package role;

import user.UserType;
import java.time.LocalTime;

/**
 *
 * @author Iustin
 */
public class Role implements IRole {
    private UserType userType ;
     private TimeSlot timeSlot;

    private Role() {
    }
     
    public Role(UserType userType, TimeSlot timeSlot){
        this.userType = userType;
        this.timeSlot = timeSlot;
    }
    /**
     * @return the userType
     */
    @Override
    public UserType getUserType() {
        return userType;
    }

    /**
     * @param userType the userType to set
     */
    @Override
    public void setUserType(UserType userType) {
        if (userType==null) {
            throw new NullPointerException();
        }
        this.userType = userType;
    }

    /**
     * @return the timeSlot
     */
    @Override
    public TimeSlot getTimeSlot() {
        return timeSlot;
    }

    /**
     * @param timeSlot the timeSlot to set
     */
    public void setTimeSlot(TimeSlot timeSlot) {
        if (timeSlot==null) {
            throw new NullPointerException();
        }
        this.timeSlot = timeSlot;
    }
    
   
    @Override
    public boolean isInTimeSlot() {
        LocalTime now = LocalTime.now();

        return !(now.isBefore(getTimeSlot().getStartTime())
                || now.isAfter(getTimeSlot().getEndTime()));

    }
            
}
