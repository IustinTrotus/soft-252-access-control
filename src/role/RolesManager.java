package role;


import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import user.UserType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Iustin
 */
public final class RolesManager {
    private final List<Role> roles = new ArrayList<>();
     
    public RolesManager() {
        
        TimeSlot noEnd = new TimeSlot(LocalTime.MIN, Duration.ofHours(23).plusMinutes(59).plusSeconds(59));
        
        TimeSlot cleaner1 = new TimeSlot(LocalTime.of(5, 30), Duration.ofHours(5));
        TimeSlot cleaner2 = new TimeSlot(LocalTime.of(17, 30), Duration.ofHours(5));
        
        TimeSlot staff = new TimeSlot(LocalTime.of(5, 30), Duration.ofHours(18).plusMinutes(29).plusSeconds(59));
        TimeSlot student = new TimeSlot(LocalTime.of(8, 30), Duration.ofHours(13).plusMinutes(30));
        TimeSlot visitor = new TimeSlot(LocalTime.of(8, 30), Duration.ofHours(13).plusMinutes(30));
        
        
        addRole(new Role(UserType.Security, noEnd));
        addRole(new Role(UserType.Manager, noEnd));
        addRole(new Role(UserType.EmergencyResponder, noEnd));
        addRole(new Role(UserType.Student, student));
        addRole(new Role(UserType.Staff, staff));
        addRole(new Role(UserType.Visitor, visitor));
        
        Role cleanerRole = new Role(UserType.ContractCleaner, cleaner1);
        cleanerRole.setTimeSlot(cleaner2);
        addRole(cleanerRole);
        
    }
    
   //todo
    public Iterator getRoles() {
        return roles.iterator();
    }

    /**
     * @param role the roles to set
     */
    private boolean addRole(Role role) {
        if (null==role) {
            throw new NullPointerException();
        }
        if (this.roles.contains(role)) {
            return false;
        }
        return this.roles.add(role);
    }
    
    public List<Role> generateRolesList(List<UserType> userTypes){
        List rolesToReturn = new ArrayList();
        if (userTypes==null) {
            throw new NullPointerException();
        }
        if (userTypes.isEmpty()) {
            throw new IllegalArgumentException("the list of user types is empty");
        }
        
        //for each user type, construct a role
        userTypes.stream().forEach((UserType userType) -> {
            RolesManager.this.roles.stream().filter((role) -> 
                    (userType == role.getUserType())).forEach((Role role) -> {
                rolesToReturn.add(role);
            });
        });
        
        return rolesToReturn;
    }
}
