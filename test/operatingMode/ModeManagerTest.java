/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operatingMode;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Iustin
 */
public class ModeManagerTest {

    IOperatingMode modeManager = new ModeManager();

    public ModeManagerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        modeManager.setOperatingMode(OperatingMode.Normal, false);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetOperatingModeNormal() {
        System.out.println("getOperatingMode Normal");

        OperatingMode expResult = OperatingMode.Normal;
        OperatingMode result = modeManager.getOperatingMode();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetOperatingModeEmergency() {
        System.out.println("getOperatingMode Emergency");
        modeManager.setOperatingMode(OperatingMode.Emergency);
        OperatingMode expResult = OperatingMode.Emergency;
        OperatingMode result = modeManager.getOperatingMode();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetOperatingMode() {
        System.out.println("setOperatingMode");

        OperatingMode expResult = OperatingMode.Emergency;
        modeManager.setOperatingMode(expResult);

        OperatingMode result = modeManager.getOperatingMode();

        assertEquals(expResult, result);

        expResult = OperatingMode.Normal;
        modeManager.setOperatingMode(expResult);

        result = modeManager.getOperatingMode();

        assertEquals(expResult, result);
    }

    @Test
    public void testSetOperatingMode_Null() {
        System.out.println("setOperatingMode Null");
        OperatingMode op = null;
        try {
            modeManager.setOperatingMode(op);
            fail("Setting a NULL for Operating Mode mode did not throw a null pointer exception");
        } catch (NullPointerException ex) {
        }
    }

    @Test
    public void testEmergencyForce_Emergency_TRUE() {
        System.out.println("testEmergencyForce Emergency, TRUE");
            //set to Emergency
        OperatingMode expResult = OperatingMode.Emergency;
        modeManager.setOperatingMode(expResult, true);

        OperatingMode result = modeManager.getOperatingMode();
        assertEquals(expResult, result); //check if it is set to Emerency

                //set to Emergency while Force Emergency is TRUE
        expResult = OperatingMode.Emergency;
        modeManager.setOperatingMode(OperatingMode.Normal);
        result = modeManager.getOperatingMode();
            //check if the Emergency mode has not beeen changed
        assertEquals(expResult, result);

    }
    
        @Test
    public void testEmergencyForce_Emergency_False() {
        System.out.println("testEmergencyForce Emergency, false");
            //set to Emergency
        OperatingMode expResult = OperatingMode.Emergency;
        modeManager.setOperatingMode(expResult, false);

        OperatingMode result = modeManager.getOperatingMode();
        assertEquals(expResult, result); //check if it is set to Emerency
 
    }
    
    
    @Test
    public void testSetOperatingMode_Normal_false(){
    
            //set to Normal
        OperatingMode expResult = OperatingMode.Normal;
        modeManager.setOperatingMode(expResult, false);
    
          //set to Normal while Force Emergency is FALSE
        expResult = OperatingMode.Normal;
        modeManager.setOperatingMode(expResult);
        OperatingMode result = modeManager.getOperatingMode();
            //check if the Normal mode has not beeen set
        assertEquals(expResult, result);
    
    }

    @Test
    public void testSetOperatingMode_Normal_TRUE() {
        System.out.println("setOperatingMode Normal Force");

        OperatingMode opMode = OperatingMode.Normal;
        try {
            modeManager.setOperatingMode(opMode, true);
            fail("Setting Normal and FRORCE for Operating Mode mode did not throw a Illegal Argument exception");
        } catch (IllegalArgumentException ex) {
        }
    }

}
