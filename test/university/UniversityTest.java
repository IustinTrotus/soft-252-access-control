/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package university;

import campus.campus.Campus;
import campus.campus.ICampus;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import utilities.IObserver;
import utilities.ISubject;

/**
 *
 * @author Iustin
 */
public class UniversityTest {

    IUniversity university;
    List<ICampus> listCampus;

    public UniversityTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        university = new University();
        university.setName("Uni Name");

        listCampus = new ArrayList<>();
        ICampus campus = new Campus();
        campus.setName("Campus 1");
        listCampus.add(campus);

        campus = new Campus();
        campus.setName("Campus 2");
        listCampus.add(campus);

        listCampus.stream().forEach((campusItem) -> {
            university.addCampus(campusItem);
        });
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testRegisterObserver() {
        System.out.println("register Observer");
        IObserver newObserver = new Campus();

        ISubject uniIsubject = (ISubject) this.university;
        Boolean result = uniIsubject.registerObserver(newObserver);

        Boolean expResult = true;
        assertEquals("an observer was not registered", expResult, result);

        expResult = false;
        result = uniIsubject.registerObserver(newObserver);
        assertEquals("an observer was registered twice", expResult, result);

    }

    @Test
    public void testRemoveObserverNull() {
        System.out.println("registerObserver Null");
        ISubject uniIsubject = (ISubject) new University();
        IObserver observer = null;
        try {
            uniIsubject.registerObserver(observer);
            fail("Setting a NULL for university observers mode did not throw a null pointer exception");
        } catch (NullPointerException ex) {
        } finally {

        }
    }

    @Test
    public void testRemoveObserver() {
        System.out.println("removeObserver");
        IObserver newObserver = new Campus();

        ISubject uniIsubject = (ISubject) this.university;
        Boolean result = uniIsubject.removeObserver(newObserver);

        Boolean expResult = false;
        assertEquals("a non existent observer was removed", expResult, result);

        //register the observer that will be removed
        uniIsubject.registerObserver(newObserver);

        expResult = true;
        result = uniIsubject.removeObserver(newObserver);
        assertEquals("an observer was  not removed", expResult, result);
    }

    @Test
    public void testNotifyObservers() {
        
        for (Iterator<IObserver> iterator =this. university.getCampuses(); iterator.hasNext();) {
            IObserver next = iterator.next();
            next.update((ISubject) this.university);
        }
        fail("what is it supposed to do?");
    }

    @Test
    public void testUpdate() {
        System.out.println("update");
        ISubject iSubject = null;
        University instance = new University();
        instance.update(iSubject);
        fail("The test case is a prototype.");
    }

    @Test
    public void testAddCampus() {
        System.out.println("addCampus");
        ICampus campus = new Campus();
        campus.setName("Campus 3 Test");

        int expResult = countCampuses(university) + 1;
        university.addCampus(campus);

        int result = countCampuses(university);
        assertEquals("Adding a campus did not increase the list by 1", expResult, result);
    }

    /**
     * remove a new campus - expect false add the campus - expect true remove
     * the campus - expect true
     */
    @Test
    public void testRemoveCampus() {
        System.out.println("removeCampus");

        ICampus campus = new Campus();
        //count the campuses before removal
        int before = countCampuses(university);

        //removing a nonexisting campus returns  false
        boolean expResult = false;
        boolean result = university.removeCampus(campus);

        //REMOVE the campus 1st time
        //count the campuses after removal
        int after = countCampuses(university);

        assertEquals(expResult, result);
        assertEquals("A non existent capus was removed", before, after);

        //ADD the campus so it can be removed later //-------------------
        expResult = true;
        result = university.addCampus(campus);
        assertEquals("the campus could not be added", expResult, result);

        //count the campuses after adding
        after = countCampuses(university);
        assertEquals("the campus might not be in the list", before + 1, after);
        //--------------------------------------------

        //REMOVE an existing campus
        expResult = true;
        result = university.removeCampus(campus);
        assertEquals("the campus could not be removed", expResult, result);

        after = countCampuses(university);
        assertEquals("incorrect nummber of campusses returned", before, after);

    }

    @Test
    public void testHasCampusTrue() {
        System.out.println("testHasCampus true");

        ICampus campus = new Campus();
        boolean expResult = true;
        university.addCampus(campus);

        boolean result = university.hasCampus(campus);
        assertEquals(expResult, result);
    }

    @Test
    public void testHasCampusFalse() {
        System.out.println("testHasCampus False");
        ICampus campus = new Campus();

        boolean expResult = false;
        boolean result = university.hasCampus(campus);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetCampuses() {
        System.out.println("get campusses iterator test");
        University university = new University();
        ICampus campus = new Campus();

        Iterator<ICampus> iterator = university.getCampuses();
        assertEquals("Iterator did not loop over the expected number of items.", false, iterator.hasNext());

        university.addCampus(campus);
        campus = new Campus();
        university.addCampus(campus);
        campus = new Campus();
        university.addCampus(campus);

        int expResult = 3;
        int result = 0;
        iterator = university.getCampuses();
        while (iterator.hasNext()) {
            iterator.next();
            result++;
        }
        assertEquals("Iterator did not loop over the expected number of items.", expResult, result);
    }

    private int countCampuses(IUniversity university) {
        int count = 0;

        for (Iterator<ICampus> iterator = university.getCampuses(); iterator.hasNext();) {
            ICampus next = iterator.next();
            count++;
        }

        return count;
    }

}
